﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Glenworx.TaskManagement.Domain.TaskManagement
{
    public class Task : Entity
    {
        public virtual Task ParentTask { get; set; }

        public virtual string Name { get; set; }

        public virtual string Description { get; set; }

        public virtual User CreatedByUser { get; set; }

        public virtual User AssignedToUser { get; set; }

        public virtual double? Estimation { get; set; }

        public virtual PriorityLevel Priority { get; set; }

        public virtual ICollection<Task> SubTasks { get; set; }

        protected Task()
        {
            Priority = PriorityLevel.Low;
            SubTasks = new List<Task>();
        }

        public Task(string name, string description, User createdByUser)
            : this()
        {
            Name = name;
            Description = description;
            CreatedByUser = createdByUser;
            CreatedDate = LastModificationDate = DateTime.UtcNow;
        }

        public void Disassociate()
        {
            AssignTo(null);
        }

        public void AssignTo(User user)
        {
            // trigger lazy load of the property
            // so the EF knows something has changed
            var dummy = AssignedToUser;

            AssignedToUser = user;
            LastModificationDate = DateTime.UtcNow;
        }

        public void Update(string name, string description, User assignToUser, PriorityLevel priority, double? estimation)
        {
            Name = name;
            Description = description;
            AssignTo(assignToUser);
            Priority = priority;
            Estimation = estimation;

            LastModificationDate = DateTime.UtcNow;
        }

        public void AddSubTask(Task task)
        {
            SubTasks.Add(task);
            LastModificationDate = DateTime.UtcNow;
        }

        public string GetDisplayNumber()
        {
            string displayNumber = Id.ToString();

            if (null != ParentTask)
            {
                Task parentTask = ParentTask;
                Task currentTask = this;
                displayNumber = String.Empty;

                while (true)
                {
                    int index = parentTask
                                    .SubTasks
                                    .OrderBy(x => x.Id)
                                    .ToList()
                                    .IndexOf(currentTask) + 1;

                    displayNumber = $".{index}{displayNumber}";

                    if(null == parentTask.ParentTask)
                    {
                        displayNumber = $"{parentTask.Id}{displayNumber}";
                        break;
                    }

                    currentTask = parentTask;
                    parentTask = parentTask.ParentTask;
                }
            }

            return displayNumber;
        }
    }
}
