﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Glenworx.TaskManagement.Domain.TaskManagement
{
    public abstract class Entity
    {
        public int Id { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime LastModificationDate { get; set; }

        public byte[] Version { get; set; }
    }
}
