﻿namespace Glenworx.TaskManagement.Domain.TaskManagement
{
    public enum PriorityLevel
    {
        Low = 1,
        Medium,
        High,
        Urgent
    }
}
