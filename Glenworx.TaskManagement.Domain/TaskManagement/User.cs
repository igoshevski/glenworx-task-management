﻿using System;

namespace Glenworx.TaskManagement.Domain.TaskManagement
{
    public class User : Entity
    {
        public string Username { get; set; }

        public string DisplayName { get; set; }

        protected User()
        { }

        public User(string username)
        {
            Username = username;
            CreatedDate = LastModificationDate = DateTime.UtcNow;
        }

        public override string ToString()
        {
            return Username + (String.IsNullOrEmpty(DisplayName) ? String.Empty : $" ({DisplayName})");
        }
    }
}
