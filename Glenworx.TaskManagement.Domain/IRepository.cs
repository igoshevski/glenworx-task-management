﻿using System;
using System.Collections.Generic;
using System.Linq;
using Glenworx.TaskManagement.Domain.TaskManagement;

namespace Glenworx.TaskManagement.Domain
{
    public interface IRepository
    {
        List<TEntity> GetAll<TEntity>() where TEntity : Entity;

        TEntity Get<TEntity>(int id) where TEntity : Entity;

        TEntity Get<TEntity>(int id, byte[] version) where TEntity : Entity;

        void Add<TEntity>(TEntity entity) where TEntity : Entity;

        void AddRange<TEntity>(IEnumerable<TEntity> list) where TEntity : Entity;

        void Delete<TEntity>(TEntity entity) where TEntity : Entity;

        void Delete<TEntity>(int id) where TEntity : Entity;

        IQueryable<TEntity> Query<TEntity>() where TEntity : Entity;
    }
}
