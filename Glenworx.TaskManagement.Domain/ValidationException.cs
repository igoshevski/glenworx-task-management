﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Glenworx.TaskManagement.Domain
{
    public class EntityValidationException : Exception
    {
        public EntityValidationException(IEnumerable<ValidationResult> validationResults)
        : base(GetFirstErrorMessage(validationResults))
        {
            Errors = new ReadOnlyCollection<ValidationResult>(validationResults.ToArray());
        }

        public ReadOnlyCollection<ValidationResult> Errors { get; private set; }

        private static string GetFirstErrorMessage(IEnumerable<ValidationResult> errors)
        {
            return errors.First().ErrorMessage;
        }
    }
}
