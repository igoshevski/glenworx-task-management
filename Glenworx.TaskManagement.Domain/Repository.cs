﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Glenworx.TaskManagement.Domain.TaskManagement;

namespace Glenworx.TaskManagement.Domain
{
    public class Repository : IRepository
    {
        private readonly DbContext _dbContext;

        public Repository(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Add<TEntity>(TEntity entity) where TEntity : Entity
        {
            if (null == entity)
            {
                throw new ArgumentNullException("entity");
            }

            _dbContext.Set<TEntity>().Add(entity);
        }

        public void AddRange<TEntity>(IEnumerable<TEntity> list) where TEntity : Entity
        {
            if (null == list)
            {
                throw new ArgumentNullException("list");
            }

            _dbContext.Set<TEntity>().AddRange(list);
        }

        public void Delete<TEntity>(TEntity entity) where TEntity : Entity
        {
            if(null == entity)
            {
                throw new ArgumentNullException("entity");
            }

            _dbContext.Set<TEntity>().Remove(entity);
        }

        public void Delete<TEntity>(int id) where TEntity : Entity
        {
            var dbSet = _dbContext.Set<TEntity>();
            var entity = dbSet.SingleOrDefault(e => e.Id == id);

            Delete(entity);
        }

        public TEntity Get<TEntity>(int id) where TEntity : Entity
        {
            return _dbContext
                        .Set<TEntity>()
                        .SingleOrDefault(e => e.Id == id);

        }

        public TEntity Get<TEntity>(int id, byte[] version) where TEntity : Entity
        {
            return _dbContext
                        .Set<TEntity>()
                        .SingleOrDefault(e => e.Id == id && e.Version == version);
        }

        public List<TEntity> GetAll<TEntity>() where TEntity : Entity
        {
            return _dbContext.Set<TEntity>().ToList();
        }

        public IQueryable<TEntity> Query<TEntity>() where TEntity : Entity
        {
            return _dbContext.Set<TEntity>();
        }
    }
}
