﻿using Glenworx.TaskManagement.Domain;
using Glenworx.TaskManagement.Domain.TaskManagement;
using Glenworx.TaskManagement.ServiceLayer.Users.Dto;
using System;

namespace Glenworx.TaskManagement.ServiceLayer.Users
{
    public class UserService : IUserService
    {
        private readonly IRepository _repository;

        public UserService(IRepository repository)
        {
            if(null == repository)
            {
                throw new ArgumentNullException("repository");
            }

            _repository = repository;
        }

        public UserDetailsDto GetUserDetails(GetUserDetailsInput input)
        {
            var user = _repository.Get<User>(input.Id);

            return user == null ?
                null :
                new UserDetailsDto
                {
                    Id = user.Id,
                    DisplayName = user.DisplayName,
                    Username = user.Username
                };
        }
    }
}
