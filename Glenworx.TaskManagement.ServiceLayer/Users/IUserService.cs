﻿using Glenworx.TaskManagement.ServiceLayer.Users.Dto;

namespace Glenworx.TaskManagement.ServiceLayer.Users
{
    public interface IUserService
    {
        UserDetailsDto GetUserDetails(GetUserDetailsInput input);
    }
}
