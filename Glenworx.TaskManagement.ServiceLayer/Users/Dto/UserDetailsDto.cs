﻿namespace Glenworx.TaskManagement.ServiceLayer.Users.Dto
{
    public class UserDetailsDto
    {
        public int Id { get; set; }

        public string Username { get; set; }

        public string DisplayName { get; set; }
    }
}
