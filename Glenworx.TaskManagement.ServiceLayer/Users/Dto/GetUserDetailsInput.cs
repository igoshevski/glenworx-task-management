﻿namespace Glenworx.TaskManagement.ServiceLayer.Users.Dto
{
    public class GetUserDetailsInput
    {
        public int Id { get; set; }
    }
}
