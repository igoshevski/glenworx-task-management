﻿using Glenworx.TaskManagement.Domain;
using Glenworx.TaskManagement.Domain.TaskManagement;
using Glenworx.TaskManagement.ServiceLayer.Tasks.Features;
using System;
using System.Data.Entity;
using System.Linq;

namespace Glenworx.TaskManagement.ServiceLayer.Tasks
{
    public class TaskService : BaseService, ITaskService
    {
        private readonly IRepository _repository;

        public TaskService(IRepository repository)
        {
            _repository = repository;
        }

        public SearchTasks.Response SearchTasks(SearchTasks.Request request)
        {
            var queryTasks = _repository
                            .Query<Task>()
                            .Where(t =>
                                    (t.ParentTask == null || false == request.OnlyRootTasks) &&
                                    (null == request.TaskName || t.Name.Contains(request.TaskName)) &&
                                    (null == request.TaskAssignedToUserId || (request.TaskAssignedToUserId == -1 && t.AssignedToUser == null) || request.TaskAssignedToUserId == t.AssignedToUser.Id) &&
                                    (null == request.TaskCreatedDate || DbFunctions.DiffDays(request.TaskCreatedDate, t.CreatedDate) == 0))
                            .OrderBy(t => t.Id);

            var queryUsers = _repository
                                .Query<User>()
                                .OrderBy(u => u.DisplayName);

            var response = new SearchTasks.Response()
            {
                Tasks = queryTasks.Select(ConstructSearchTasksResponseTask).ToList(),
                Users = queryUsers.Select(ConstructSearchTasksResponseUser).ToList()
            };

            return response;
        }

        public CreateTask.Response CreateTask(CreateTask.Request request)
        {
            Validate(request);

            var newTask = PrepareTask(request);

            if (CheckIfAlreadyExists(null, null, request.TaskName))
            {
                throw new ArgumentException($"A task with name [{request.TaskName}] already exists.");
            }

            _repository.Add<Task>(newTask);
            CurrentDbContext.SaveChanges();

            var response = new CreateTask.Response { CreatedTaskId = newTask.Id };

            return response;
        }

        public UpdateTask.Response UpdateTask(UpdateTask.Request request)
        {
            Validate(request);

            Task task = _repository.Get<Task>(request.TaskId);
            User assignToUser = null;

            if(null == task)
            {
                throw new ArgumentOutOfRangeException("request.TaskId");
            }

            if(request.AssignedToUserId.HasValue)
            {
                assignToUser = _repository.Get<User>(request.AssignedToUserId.Value);

                if(null == assignToUser)
                {
                    throw new ArgumentOutOfRangeException("request.AssignToUserId");
                }
            }

            if (CheckIfAlreadyExists(task.Id, null, request.TaskName))
            {
                throw new ArgumentException($"A task with name [{request.TaskName}] already exists.");
            }

            task.Update(
                request.TaskName,
                request.TaskDescription,
                assignToUser,
                request.PriorityLevel,
                request.Estimation);
            CurrentDbContext.SaveChanges();

            var response = new UpdateTask.Response { TaskId = task.Id };

            return response;
        }

        public CreateSubTask.Response CreateSubTask(CreateSubTask.Request request)
        {
            Validate(request);

            Task parentTask = null;
            
            if (null == (parentTask = _repository.Get<Task>(request.ParentTaskId)))
            {
                throw new ArgumentOutOfRangeException("request.ParentTaskId");
            }

            var newTask = PrepareTask(request);

            if (CheckIfAlreadyExists(null, parentTask, request.TaskName))
            {
                throw new ArgumentException($"A task with name [{request.TaskName}] already exists.");
            }

            parentTask.AddSubTask(newTask);
            CurrentDbContext.SaveChanges();

            var response = new CreateSubTask.Response { CreatedTaskId = newTask.Id };

            return response;
        }

        public AssignTask.Response AssignTask(AssignTask.Request request)
        {
            Validate(request);

            User user = null;
            Task task = null;

            if(request.AssignToUserId.HasValue && 
                null == (user = _repository.Get<User>(request.AssignToUserId.Value)))
            {
                throw new ArgumentOutOfRangeException("request.AssignToUserId");
            }

            if (null == (task = _repository.Get<Task>(request.TaskId)))
            {
                throw new ArgumentOutOfRangeException("request.TaskId");
            }

            if (null == user)
            {
                task.Disassociate();
            }
            else
            {
                task.AssignTo(user);
            }

            CurrentDbContext.SaveChanges();

            var response = new AssignTask.Response();

            return response;
        }

        public GetTaskDetails.Response GetTaskDetails(GetTaskDetails.Request request)
        {
            Validate(request);

            var task = _repository.Get<Task>(request.TaskId);

            if(null == task)
            {
                throw new ArgumentOutOfRangeException("request.TaskId");
            }

            var response = new GetTaskDetails.Response
            {
                TaskId = task.Id,
                TaskName = task.Name,
                TaskDescription = task.Description,
                AssignedToUserId = task.AssignedToUser?.Id,
                Estimation = task.Estimation,
                PriorityLevel = task.Priority
            };

            return response;
        }

        private Task PrepareTask(CreateTask.Request request)
        {
            User createdByUser = null;
            User assignedToUser = null;

            if (null == (createdByUser = _repository.Get<User>(request.CreatedByUserId)))
            {
                throw new ArgumentOutOfRangeException("request.CreatedByUserId");
            }

            if (request.AssignedToUserId.HasValue &&
                null == (assignedToUser = _repository.Get<User>(request.AssignedToUserId.Value)))
            {
                throw new ArgumentOutOfRangeException("request.AssignedToUserId");
            }

            var newTask = new Task(request.TaskName, request.TaskDescription, createdByUser)
            {
                Estimation = request.Estimation,
                AssignedToUser = assignedToUser,
                Priority = request.PriorityLevel
            };

            return newTask;
        }

        private bool CheckIfAlreadyExists(int? id, Task parentTask, string taskName)
        {
            int? parentTaskId = parentTask?.Id;

            return _repository
                    .Query<Task>()
                    .Any(t => (null == id || t.Id != id) &&
                                ((t.ParentTask == null && parentTaskId == null) || (t.ParentTask != null && parentTaskId != null && t.ParentTask.Id == parentTaskId)) &&
                                t.Name.ToLower() == taskName.ToLower());
        }

        private static SearchTasks.Response.Task ConstructSearchTasksResponseTask(Task task)
        {
            return new SearchTasks.Response.Task
            {
                Id = task.Id,
                DisplayNumber = task.GetDisplayNumber(),
                Priority = task.Priority.ToString(),
                Name = task.Name,
                CreatedBy = task.CreatedByUser.ToString(),
                AssignedTo = task.AssignedToUser?.ToString(),
                Estimation = task.Estimation,
                SubTasks = task.SubTasks.Select(ConstructSearchTasksResponseTask).ToList()
            };            
        }

        private static SearchTasks.Response.User ConstructSearchTasksResponseUser(User user)
        {
            return new SearchTasks.Response.User
            {
                Id = user.Id,
                DisplayName = user.ToString()
            };
        }
    }
}
