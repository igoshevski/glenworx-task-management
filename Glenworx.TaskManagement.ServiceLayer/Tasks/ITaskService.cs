﻿using Glenworx.TaskManagement.ServiceLayer.Tasks.Features;

namespace Glenworx.TaskManagement.ServiceLayer.Tasks
{
    public interface ITaskService
    {
        SearchTasks.Response SearchTasks(SearchTasks.Request request);

        CreateTask.Response CreateTask(CreateTask.Request request);

        UpdateTask.Response UpdateTask(UpdateTask.Request request);

        CreateSubTask.Response CreateSubTask(CreateSubTask.Request request);

        AssignTask.Response AssignTask(AssignTask.Request request);

        GetTaskDetails.Response GetTaskDetails(GetTaskDetails.Request request);
    }
}
