﻿using Glenworx.TaskManagement.Domain.TaskManagement;
using System.ComponentModel.DataAnnotations;

namespace Glenworx.TaskManagement.ServiceLayer.Tasks.Features
{
    public class CreateTask
    {
        public class Request
        {
            [Required]
            public string TaskName { get; set; }

            public string TaskDescription { get; set; }

            [Required]
            public int CreatedByUserId { get; set; }

            public int? AssignedToUserId { get; set; }

            public PriorityLevel PriorityLevel { get; set; }

            [Range(0.01, 999)]
            public double? Estimation { get; set; }
        }

        public class Response
        {
            public int CreatedTaskId { get; set; }
        }
    }
}
