﻿using System.ComponentModel.DataAnnotations;

namespace Glenworx.TaskManagement.ServiceLayer.Tasks.Features
{
    public class AssignTask
    {
        public class Request
        {
            [Required]
            public int TaskId { get; set; }

            public int? AssignToUserId { get; set; }
        }

        public class Response
        { }
    }
}
