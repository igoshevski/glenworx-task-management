﻿using Glenworx.TaskManagement.Domain.TaskManagement;
using System.ComponentModel.DataAnnotations;

namespace Glenworx.TaskManagement.ServiceLayer.Tasks.Features
{
    public class GetTaskDetails
    {
        public class Request
        {
            [Required]
            public int TaskId { get; set; }
        }

        public class Response
        {
            public int TaskId { get; set; }

            public string TaskName { get; set; }

            public string TaskDescription { get; set; }

            public int? AssignedToUserId { get; set; }

            public PriorityLevel PriorityLevel { get; set; }

            public double? Estimation { get; set; }
        }
    }
}
