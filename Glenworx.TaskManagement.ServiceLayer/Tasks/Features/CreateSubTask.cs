﻿using System.ComponentModel.DataAnnotations;

namespace Glenworx.TaskManagement.ServiceLayer.Tasks.Features
{
    public class CreateSubTask
    {
        public class Request : CreateTask.Request
        {
            [Required]
            public int ParentTaskId { get; set; }
        }

        public class Response : CreateTask.Response
        { }
    }
}
