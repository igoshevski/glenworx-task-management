﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Glenworx.TaskManagement.ServiceLayer.Tasks.Features
{
    public class SearchTasks
    {
        public class Request
        {
            public string TaskName { get; set; }

            public int? TaskAssignedToUserId { get; set; }

            public DateTime? TaskCreatedDate { get; set; }

            public string TaskCreatedDateString
            {
                get
                {
                    return (null == TaskCreatedDate ? String.Empty : TaskCreatedDate.Value.ToShortDateString());
                }
            }

            public bool OnlyRootTasks
            {
                get
                {
                    return String.IsNullOrWhiteSpace(TaskName) &&
                            false == TaskAssignedToUserId.HasValue &&
                            false == TaskCreatedDate.HasValue;
                }
            }
        }

        public class Response
        {
            public IEnumerable<Task> Tasks { get; set; } = new List<Task>();

            public IEnumerable<User> Users { get; set; } = new List<User>();

            public class Task
            {
                public int Id { get; set; }

                public string DisplayNumber { get; set; }

                public string Priority { get; set; }

                public string Name { get; set; }

                public string CreatedBy { get; set; }

                public string AssignedTo { get; set; }

                public double? Estimation { get; set; }

                public IEnumerable<Task> SubTasks { get; set; } = new List<Task>();
            }

            public class User
            {
                public int Id { get; set; }

                public string DisplayName { get; set; }
            }
        }
    }
}
