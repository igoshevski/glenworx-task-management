﻿using StructureMap.Attributes;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using Glenworx.TaskManagement.Domain;
using System;

namespace Glenworx.TaskManagement.ServiceLayer
{
    public abstract class BaseService
    {
        [SetterProperty]
        public DbContext CurrentDbContext { get; set; }

        protected void Validate(object request)
        {
            if(null == request)
            {
                throw new ArgumentNullException("request");
            }

            var context = new ValidationContext(request);
            var validationResults = new List<ValidationResult>();
            var invalid = (false == Validator.TryValidateObject(request, context, validationResults));

            if(invalid)
            {
                throw new EntityValidationException(validationResults);
            }
        }
    }
}
