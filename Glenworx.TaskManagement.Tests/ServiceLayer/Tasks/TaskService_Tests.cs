﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Glenworx.TaskManagement.ServiceLayer.Tasks;
using Glenworx.TaskManagement.ServiceLayer.Tasks.Features;
using Glenworx.TaskManagement.Domain.TaskManagement;
using Glenworx.TaskManagement.Domain;
using System.Linq;
using System.Data.SqlClient;

namespace Glenworx.TaskManagement.Tests.ServiceLayer.Tasks
{
    [TestClass]
    public class TaskService_Tests : TestBase
    {
        private readonly ITaskService _taskService;

        public TaskService_Tests()
        {
            _taskService = new TaskService(Repository) { CurrentDbContext = CurrentDbContext };
        }

        [TestMethod]
        public void CreateTask_ValidCreateUser_Created()
        {
            // arrange
            var testUser = Repository.Query<User>().First();

            var request = new CreateTask.Request
            {
                TaskName = "Test insert of valid task #1",
                TaskDescription = "Lorem Ipsum",
                CreatedByUserId = testUser.Id
            };

            // act
            var response = _taskService.CreateTask(request);

            // assert
            Assert.AreNotEqual(0, response.CreatedTaskId);
            var task = Repository.Get<Task>(response.CreatedTaskId);
            Assert.AreEqual(request.TaskName, task.Name);
            Assert.AreEqual(request.TaskDescription, task.Description);
            Assert.AreEqual(request.CreatedByUserId, task.CreatedByUser.Id);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void CreateTask_InvalidCreateUser_ExceptionThrown()
        {
            // arrange
            var request = new CreateTask.Request
            {
                TaskName = "Test insert of valid task #1",
                TaskDescription = "Lorem Ipsum",
                CreatedByUserId = 99
            };

            // act
            var response = _taskService.CreateTask(request);
        }

        [TestMethod]
        [ExpectedException(typeof(EntityValidationException))]
        public void CreateTask_InvalidData_ExceptionThrown()
        {
            // arrange
            var request = new CreateTask.Request();

            // act
            var response = _taskService.CreateTask(request);
        }

        [TestMethod]
        public void UpdateTask_ValidDataWithoutAssignToUser_Updated()
        {
            // arrange
            var existingTask = Repository.Query<Task>().First();
            User newAssignedToUser = null;

            var request = new UpdateTask.Request
            {
                TaskId = existingTask.Id,
                TaskName = $"{existingTask.Name} - Updated",
                TaskDescription = $"{existingTask.Description} - Updated",
                AssignedToUserId = newAssignedToUser?.Id
            };

            // act
            _taskService.UpdateTask(request);

            // assert
            var updatedTask = Repository.Get<Task>(request.TaskId);
            Assert.AreEqual(request.TaskName, updatedTask.Name);
            Assert.AreEqual(request.TaskDescription, updatedTask.Description);
            Assert.AreEqual(newAssignedToUser, updatedTask.AssignedToUser);
        }

        [TestMethod]
        public void CreateSubTask_ValidParentTask_Created()
        {
            // arrange
            var testUser = Repository.Query<User>().First();
            var testParentTask = Repository.Query<Task>().First();
            var request = new CreateSubTask.Request
            {
                ParentTaskId = testParentTask.Id,
                TaskName = "Test Sub Task #1",
                TaskDescription = "Lorem Ipsum",
                CreatedByUserId = testUser.Id
            };

            // act
            var response = _taskService.CreateSubTask(request);

            // assert
            Assert.AreNotEqual(0, response.CreatedTaskId);

            var task = Repository.Get<Task>(response.CreatedTaskId);
            Assert.AreEqual(request.ParentTaskId, task.ParentTask?.Id);            
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void CreateTask_ExistingNameAtRootLevel_ExceptionThrown()
        {
            // arrange
            var testUser = Repository.Query<User>().First();
            var testTask = Repository.Query<Task>().First();

            var request = new CreateTask.Request
            {
                TaskName = testTask.Name,
                CreatedByUserId = testUser.Id
            };

            // act
            _taskService.CreateTask(request);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void CreateSubTask_ExistingNameAtSameLevel_ExceptionThrown()
        {
            // arrange
            var testUser = Repository.Query<User>().First();
            var testTask = Repository.Query<Task>().First();

            var request = new CreateSubTask.Request
            {
                TaskName = testTask.SubTasks.First().Name,
                CreatedByUserId = testUser.Id,
                ParentTaskId = testTask.Id
            };

            // act
            _taskService.CreateSubTask(request);
        }
    }
}
