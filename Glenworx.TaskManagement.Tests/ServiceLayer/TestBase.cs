﻿using Glenworx.TaskManagement.Domain;
using Glenworx.TaskManagement.Domain.TaskManagement;
using Glenworx.TaskManagement.Infrastructure.TaskManagement.DataAccess;
using System.Data.Entity;

namespace Glenworx.TaskManagement.Tests.ServiceLayer
{
    public abstract class TestBase
    {
        protected readonly DbContext CurrentDbContext;
        protected readonly IRepository Repository;

        protected TestBase()
        {
            var dbConnection = Effort.DbConnectionFactory.CreateTransient();
            CurrentDbContext = new TaskManagementDbContext(dbConnection);
            Repository = new Repository(CurrentDbContext);

            // TestData
            var testUser = new User("john.doe");
            var testTask = new Task("TestData task #1", "Lorem Ipsum", testUser);
            var nestedTask = new Task("TestData task #1.1", string.Empty, testUser);
            testTask.AddSubTask(nestedTask);

            CurrentDbContext.Set<User>().Add(testUser);
            CurrentDbContext.Set<Task>().Add(testTask);

            CurrentDbContext.SaveChanges();
        }
    }
}
