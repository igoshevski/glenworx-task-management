namespace Glenworx.TaskManagement.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTask : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Tasks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 100),
                        Description = c.String(maxLength: 500),
                        CreatedDate = c.DateTime(nullable: false),
                        LastModificationDate = c.DateTime(nullable: false),
                        Version = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        AssignedToUserId = c.Int(),
                        CreatedByUserId = c.Int(nullable: false),
                        ParentTaskId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.AssignedToUserId)
                .ForeignKey("dbo.Users", t => t.CreatedByUserId, cascadeDelete: true)
                .ForeignKey("dbo.Tasks", t => t.ParentTaskId)
                .Index(t => new { t.Name, t.ParentTaskId }, unique: true, name: "UX_Tasks_Name_ParentTaskId")
                .Index(t => t.AssignedToUserId)
                .Index(t => t.CreatedByUserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tasks", "ParentTaskId", "dbo.Tasks");
            DropForeignKey("dbo.Tasks", "CreatedByUserId", "dbo.Users");
            DropForeignKey("dbo.Tasks", "AssignedToUserId", "dbo.Users");
            DropIndex("dbo.Tasks", new[] { "CreatedByUserId" });
            DropIndex("dbo.Tasks", new[] { "AssignedToUserId" });
            DropIndex("dbo.Tasks", "UX_Tasks_Name_ParentTaskId");
            DropTable("dbo.Tasks");
        }
    }
}
