namespace Glenworx.TaskManagement.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateTask_AdditionalDetails : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tasks", "Estimation", c => c.Double());
            AddColumn("dbo.Tasks", "Priority", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tasks", "Priority");
            DropColumn("dbo.Tasks", "Estimation");
        }
    }
}
