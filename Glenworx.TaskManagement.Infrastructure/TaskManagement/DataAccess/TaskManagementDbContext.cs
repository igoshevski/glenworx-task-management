using Glenworx.TaskManagement.Infrastructure.TaskManagement.DataAccess.Repositories.Mappings;
using System.Data.Common;
using System.Data.Entity;

namespace Glenworx.TaskManagement.Infrastructure.TaskManagement.DataAccess
{
    public class TaskManagementDbContext : DbContext
    {
        public TaskManagementDbContext()
            : base("name=Default")
        {
            Database.SetInitializer(new TaskManagementDbContextInitializer());
        }

        public TaskManagementDbContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        { }

        public TaskManagementDbContext(DbConnection connection)
            : base(connection, true)
        { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            new EntityConfiguration().Init(modelBuilder);

            base.OnModelCreating(modelBuilder);
        }
    }
}