﻿using Glenworx.TaskManagement.Domain.TaskManagement;
using System;
using System.Collections.Generic;
using System.Data.Entity;

namespace Glenworx.TaskManagement.Infrastructure.TaskManagement.DataAccess
{
    public class TaskManagementDbContextInitializer : DropCreateDatabaseIfModelChanges<TaskManagementDbContext>
    {
        protected override void Seed(TaskManagementDbContext context)
        {
            var users = new List<User>
            {
                new User("john.appleseed") { DisplayName = "John Appleseed" },
                new User("john.doe") { DisplayName = "John Doe" }
            };

            context.Set<User>().AddRange(users);

            var tasks = new List<Task>
            {
                new Task("Sample Task 1 - Buy coffee for the office", String.Empty, users[0]),
                new Task("Sample Task 2 - Implement IoC/DI", String.Empty, users[0])
                {
                    AssignedToUser = users[0],
                    Estimation = 6.5
                },
                new Task("Sample Task 3 - Extend Users service", "Update service to send email when user changes passwrod", users[1])
                {
                    AssignedToUser = users[0],
                    Estimation = 3
                }
            };

            var taskWithSubtasks = tasks[0];
            taskWithSubtasks.AddSubTask(new Task("Sample Task 1 Breakdown - Get out of the office", String.Empty, users[0]));
            taskWithSubtasks.AddSubTask(new Task("Sample Task 1 Breakdown - Go to the nearest store", String.Empty, users[0]));

            context.Set<Task>().AddRange(tasks);

            base.Seed(context);
        }
    }
}
