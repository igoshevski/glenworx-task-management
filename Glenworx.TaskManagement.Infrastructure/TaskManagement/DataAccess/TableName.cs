﻿namespace Glenworx.TaskManagement.Infrastructure.TaskManagement.DataAccess
{
    public class TableName
    {
        public const string Users = "Users";
        public const string Tasks = "Tasks";
    }
}
