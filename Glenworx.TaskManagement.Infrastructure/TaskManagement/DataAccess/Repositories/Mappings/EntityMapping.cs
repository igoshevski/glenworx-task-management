﻿using Glenworx.TaskManagement.Domain.TaskManagement;
using System.Data.Entity.ModelConfiguration;

namespace Glenworx.TaskManagement.Infrastructure.TaskManagement.DataAccess.Repositories.Mappings
{
    public class EntityMapping <TEntity> : EntityTypeConfiguration<TEntity> where TEntity : Entity
    {
        public EntityMapping()
        {
            HasKey(e => e.Id);
            Property(e => e.Version).IsRowVersion();
        }
    }
}
