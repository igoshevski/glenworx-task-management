﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Reflection;

namespace Glenworx.TaskManagement.Infrastructure.TaskManagement.DataAccess.Repositories.Mappings
{
    public class EntityConfiguration
    {
        public void Init(DbModelBuilder modelBuilder)
        {
            Assembly
                .GetExecutingAssembly()
                .GetTypes()
                .Where(t => null != t.BaseType &&
                            t.BaseType.IsGenericType &&
                            t.BaseType.GetGenericTypeDefinition() == typeof(EntityMapping<>))
                .ToList()
                .ForEach(t =>
                {
                    dynamic i = Activator.CreateInstance(t);
                    modelBuilder.Configurations.Add(i);
                });
        }
    }
}
