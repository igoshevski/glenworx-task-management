﻿using Glenworx.TaskManagement.Domain.TaskManagement;

namespace Glenworx.TaskManagement.Infrastructure.TaskManagement.DataAccess.Repositories.Mappings
{
    public class UserMapping : EntityMapping<User>
    {
        public UserMapping()
        {
            ToTable(TableName.Users);

            Property(e => e.Username).HasMaxLength(50);
            Property(e => e.DisplayName).HasMaxLength(50);
        }
    }
}
