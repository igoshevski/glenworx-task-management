﻿using Glenworx.TaskManagement.Domain.TaskManagement;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Linq;
using System.Text;

namespace Glenworx.TaskManagement.Infrastructure.TaskManagement.DataAccess.Repositories.Mappings
{
    public class TaskMapping : EntityMapping<Task>
    {
        public TaskMapping()
        {
            ToTable(TableName.Tasks);

            Property(e => e.Name)
                .HasMaxLength(100)
                .HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                    new IndexAnnotation(new IndexAttribute("UX_Tasks_Name_ParentTaskId", 1) { IsUnique = true }));

            Property(e => e.Description).HasMaxLength(500);

            HasOptional(e => e.AssignedToUser)
                .WithMany()
                .Map(x => x.MapKey("AssignedToUserId"));

            HasRequired(e => e.CreatedByUser)
                .WithMany()
                .Map(x => x.MapKey("CreatedByUserId"));

            HasMany(e => e.SubTasks)
                .WithOptional(e => e.ParentTask)
                .Map(x => 
                    x.MapKey("ParentTaskId")
                        .HasColumnAnnotation(
                            "ParentTaskId",
                            IndexAnnotation.AnnotationName,
                            new IndexAnnotation(new IndexAttribute("UX_Tasks_Name_ParentTaskId", 2) { IsUnique = true })));
        }
    }
}
