﻿; (function (window, $, undefined) {
    'use strict';

    var $addEditTaskModal,

        $assignUserModal,

        clearValidationMsgs = function (e) {
            $(this).validate().resetForm();
        },

        resetSubmitButton = function ($modal) {
            $modal.find('#btn-submit').removeClass('disabled').text('Submit');
        },

        submitTaskDetailsSuccess = function (response) {
            $addEditTaskModal.modal('hide');
            window.location.reload();
        },

        submitTaskDetails = function (e) {
            var $form = $addEditTaskModal.find('form');

            e.preventDefault();

            if ($form.valid()) {
                $addEditTaskModal
                    .find('#btn-submit')
                        .addClass('disabled')
                    .text('Submitting...');

                $.post($form.prop('action'), $addEditTaskModal.find('form').serialize())
                    .done(submitTaskDetailsSuccess)
                    .fail(showErrorOccured)
                    .always(function () { resetSubmitButton($addEditTaskModal); });
            }
        },

        assignUserToTask = function (e) {
            e.preventDefault();

            $assignUserModal
                .find('#btn-submit')
                .addClass('disabled')
                .text('Submitting...');

            $.post(Action.Home.AssignTask, $assignUserModal.find('form').serialize())
                .done(function () { $assignUserModal.modal('hide'); window.location.reload(); })
                .fail(showErrorOccured)
                .always(function () { resetSubmitButton($assignUserModal); });
        },

        showErrorOccured = function () {
            alert('Error occured');
        },

        retrieveTaskDetails = function (taskId) {
            $.get(Action.Home.GetTaskDetails, { taskId: taskId })
                .done(loadTaskDataToDialog)
                .fail(showErrorOccured);
        },

        loadTaskDataToDialog = function (response) {
            var $form = $addEditTaskModal.find('form');

            $form.find('#task-name').val(response.taskName);
            $form.find('#task-description').val(response.taskDescription);
            $form.find('#assigned-to-user-id').val(response.assignedToUserId);
            $form.find('#priority-level').val(response.priorityLevel);
            $form.find('#estimation').val(response.estimation);
        },

        refreshDialogUI = function (e) {
            var $button = $(e.relatedTarget),
                $form = $addEditTaskModal.find('form'),
                action = $button.data('action'),
                url,
                modalTitle,
                parentTaskId = '',
                taskId = '';

            $form.trigger('reset');
            resetSubmitButton($addEditTaskModal);

            switch (action) {
                case 'create': {
                    modalTitle = 'Create Task';
                    url = Action.Home.CreateTask;
                    break;
                }
                case 'create-sub': {
                    modalTitle = 'Create Sub-Task [' + $button.data('parent-name') + ']';
                    parentTaskId = $button.data('parent-id');
                    url = Action.Home.CreateSubTask;
                    break;
                }
                case 'edit': {
                    modalTitle = 'Edit Task';
                    url = Action.Home.UpdateTask;
                    taskId = $button.data('task-id');
                    retrieveTaskDetails(taskId);
                    break;
                }
            }

            $addEditTaskModal.find('.modal-header .modal-title').text(modalTitle);
            $form.find('#parent-task-id').val(parentTaskId);
            $form.find('#task-id').val(taskId);
            $form.prop({ action: url });
        },

        refreshAssignToDialogUI = function (e) {
            var $button = $(e.relatedTarget),
                taskId = $button.data('task-id'),
                taskName = $button.data('task-name'),
                modalTitle = taskName;


            $assignUserModal.find('.modal-header .modal-title').text(modalTitle);
            $assignUserModal.find('form #task-id').val(taskId);
        },

        applyJQueryEvents = function () {
            $addEditTaskModal.on('show.bs.modal', refreshDialogUI);

            $addEditTaskModal.find('form')
                .on('submit', submitTaskDetails)
                .on('reset', clearValidationMsgs);

            $addEditTaskModal
                .find('#btn-submit')
                .on('click', function (e) { $addEditTaskModal.find('form').trigger('submit'); });

            $assignUserModal.on('show.bs.modal', refreshAssignToDialogUI);

            $assignUserModal.find('form')
                .on('submit', assignUserToTask);

            $assignUserModal
                .find('#btn-submit')
                .on('click', function (e) { $assignUserModal.find('form').trigger('submit'); });
        },

        init = function () {
            $addEditTaskModal = $('#add-edit-task-modal');
            $assignUserModal = $('#assign-user-modal');

            applyJQueryEvents();
        };

    $(function () {
        init();
    });
}(window, jQuery));