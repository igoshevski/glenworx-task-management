﻿using Glenworx.TaskManagement.ServiceLayer.Tasks;
using Glenworx.TaskManagement.ServiceLayer.Tasks.Features;
using Glenworx.TaskManagement.Web.ViewModels;
using System.Web.Mvc;

namespace Glenworx.TaskManagement.Web.Controllers
{
    public class HomeController : BaseController
    {
        private readonly ITaskService _taskService;

        public HomeController(ITaskService taskService)
        {
            _taskService = taskService;
        }

        public ActionResult Index(SearchTasks.Request request)
            => View(new IndexViewModel { Request = request, Response = _taskService.SearchTasks(request) });

        [HttpGet]
        public ActionResult GetTaskDetails(GetTaskDetails.Request request)
            => Json(_taskService.GetTaskDetails(request));

        [HttpPost]
        public ActionResult CreateTask(CreateTask.Request request)
            => Json(_taskService.CreateTask(request));

        [HttpPost]
        public ActionResult UpdateTask(UpdateTask.Request request)
            => Json(_taskService.UpdateTask(request));

        [HttpPost]
        public ActionResult CreateSubTask(CreateSubTask.Request request)
            => Json(_taskService.CreateSubTask(request));

        [HttpPost]
        public ActionResult AssignTask(AssignTask.Request request)
            => Json(_taskService.AssignTask(request));
    }
}