﻿using System.Web.Mvc;

namespace Glenworx.TaskManagement.Web.Controllers
{
    public abstract class BaseController : Controller
    {
        protected new JsonResult Json(object data)
        {
            return new JsonCamelCaseResult(data, JsonRequestBehavior.AllowGet);
        }

        protected new JsonResult Json(object data, JsonRequestBehavior behavior)
        {
            return new JsonCamelCaseResult(data, JsonRequestBehavior.AllowGet);
        }
    }
}