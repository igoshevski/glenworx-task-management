﻿using Glenworx.TaskManagement.ServiceLayer.Tasks.Features;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Glenworx.TaskManagement.Web.ViewModels
{
    public class IndexViewModel
    {
        public SearchTasks.Request Request { get; set; } = new SearchTasks.Request();

        public SearchTasks.Response Response { get; set; } = new SearchTasks.Response();

        public List<SelectListItem> GetUsersForDropdown()
        {
            return null == Response || null == Response.Users ?
                    new List<SelectListItem>() :
                    Response
                        .Users
                        .Select(x => new SelectListItem
                        {
                            Text = x.DisplayName,
                            Value = x.Id.ToString(),
                            Selected = (Request.TaskAssignedToUserId == x.Id)
                        })
                        .ToList();
        }
    }
}